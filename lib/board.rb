class Board
  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
    @winning_mark = nil
  end
  attr_accessor :grid, :winning_mark

  def place_mark(position, mark)
    if empty?(position)
      x = position[0]
      y = position[1]
      grid[x][y] = mark
    else
      raise 'position taken'
    end
  end

  def empty?(position)
    x = position[0]
    y = position[1]
    grid[x][y] ? false : true
  end

  def winner
    if diagonal_winner?
      winning_mark
    elsif row_winner?
      winning_mark
    elsif column_winner?
      winning_mark
    else
      nil
    end
  end

  def over?
    return true if winner
    grid.each do |row|
      return false if row.any? { |sym| sym == nil }
    end
    true
  end

  private

  def diagonal_winner?
    @winning_mark = grid[1][1]
    left_diag = [grid[0][0], grid[1][1], grid[2][2]]
    right_diag = [grid[0][2], grid[1][1], grid[2][0]]
    left_diag.all? { |sym| sym == winning_mark && winning_mark } ||
    right_diag.all? { |sym| sym == winning_mark && winning_mark }
  end

  def row_winner?
    grid.each_with_index do |row, i|
      @winning_mark = row[i]
      return true if row.all? { |sym| sym == winning_mark && winning_mark }
    end
    false
  end

  def column_winner?
    grid.each_index do |i|
      column = [grid[0][i], grid[1][i], grid[2][i]]
      @winning_mark = column[i]
      return true if column.all? { |sym| sym == winning_mark && winning_mark }
    end
    false
  end

end
