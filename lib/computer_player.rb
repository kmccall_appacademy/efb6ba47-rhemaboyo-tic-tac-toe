class ComputerPlayer
  def initialize(name, mark = nil)
    @name = name
    @mark = mark
    @winning_position = nil
  end
  attr_accessor :name, :mark, :winning_position

  def display(board)
    @board = board
  end

  def board
    @board
  end

  def get_move
    grid = board.grid

    if right_diag_winner?(grid)
      winning_position
    elsif left_diag_winner?(grid)
      winning_position
    elsif row_winner?(grid)
      winning_position
    elsif column_winner?(grid)
      winning_position
    else
      [(2 * rand).round, (2 * rand).round]
    end
  end

  private

  def right_diag_winner?(grid)
    right_diag = [grid[0][2], grid[1][1], grid[2][0]]
    right_diag.each_with_index do |box, idx|
      x = idx
      y = (idx - 2).abs
      if box == nil && right_diag.count { |sym| sym == mark } == 2
        @winning_position = [x, y]
        return true
      end
    end
    false
  end

  def left_diag_winner?(grid)
    left_diag = [grid[0][0], grid[1][1], grid[2][2]]
    left_diag.each_with_index do |box, idx|
      if box == nil && left_diag.count { |sym| sym == mark } == 2
        @winning_position = [idx, idx]
        return true
      end
    end
    false
  end

  def row_winner?(grid)
    grid.each_with_index do |row, x|
      y = row.index(nil)
      @winning_position = [x, y]
      return true if row.count { |sym| sym == mark } == 2 && y
    end
    false
  end

  def column_winner?(grid)
    grid.each_index do |y|
      column = [grid[0][y], grid[1][y], grid[2][y]]
      x = column.index(nil)
      @winning_position = [x, y]
      return true if column.count { |sym| sym == mark } == 2 && x
    end
    false
  end

end
