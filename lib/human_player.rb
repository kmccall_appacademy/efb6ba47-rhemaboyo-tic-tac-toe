#require 'byebug'
class HumanPlayer
  def initialize(name, mark = nil)
    @name = name
    @mark = mark
  end
  attr_accessor :name, :mark

  def get_move
    puts 'Where do you want to move?'
    input = gets.chomp
    if input[1..2] == ', ' && '001122'.count(input[0]+input[3]) == 2
      [input[0].to_i, input[3].to_i]
    else
      raise 'Please use proper format ie: 0, 2'
    end
  end

  def display(board)
    p board
  end
end
